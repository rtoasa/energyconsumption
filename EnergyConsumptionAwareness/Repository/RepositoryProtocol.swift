//
//  RepositoryProtocol.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 24/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
protocol RepositoryProtocol {
    func saveCategory()
    func loadCategory()
    func saveAdvices()
    func loadAdvices()
    func saveGeneralPoints()
    func loadGeneralPoints()
    func loadLanguage()
    func saveLanguage()
}
