//
//  ArchivingRepository.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 24/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

class ArchivingRepository:RepositoryProtocol {
    //singleton repository
    static let repository = ArchivingRepository() //shared Instance
    fileprivate init () {}
    var language = [Language]()
    var categ = [Category]()
    var generalPoint = [GeneralPoints]()
    var advic = [Advice]()
    
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    
    func saveCategory() {
       documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("category.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(categ, toFile: path) {
            print("Successfully saved category")
        } else {
            print("Failure saving category")
        }
    }
    
    func loadCategory() {
       documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("category.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Category]{
            categ = newData
        } else {
            print("File Category not found")
        }
    }
 
    func saveAdvices() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("advices.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(advic, toFile: path) {
            print("Successfully saved advices")
        } else {
            print("Failure saving advices")
        }
    }
    
    func loadAdvices() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("advices.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Advice]{
            advic = newData
        } else {
            print("File Advice not found")
        }
    }
    
    
    func saveGeneralPoints() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("generalPoints.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(generalPoint, toFile: path) {
            print("Successfully saved Gerneral Points")
        } else {
            print("Failure saving Gerneral Points")
        }
    }
    
    func loadGeneralPoints() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("generalPoints.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
    
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [GeneralPoints]{
            generalPoint = newData
        } else {
            print("File General Points not found")
        }
    }
    func saveLanguage() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("language.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(language, toFile: path) {
            print("Successfully saved languages")
        } else {
            print("Failure saving languages")
        }
    }
    
    func loadLanguage() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        //print(documentsPath)
        filePath = documentsPath.appendingPathComponent("language.data", isDirectory: false)
        //print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Language]{
            language = newData
        } else {
            print("File Language not found")
        }
    }
}
