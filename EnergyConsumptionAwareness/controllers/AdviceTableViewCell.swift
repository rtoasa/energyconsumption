//
//  AdviceTableViewCellController.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit

class AdviceTableViewCell: UITableViewCell {

    @IBOutlet weak var advice: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    var advic:Advice? {
        didSet {
            self.advice.text = advic?.advice
            self.date.text = advic?.dateTime
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
