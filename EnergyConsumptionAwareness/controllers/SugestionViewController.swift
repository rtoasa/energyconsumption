//
//  SugestionViewController.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 26/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit

class SugestionViewController: UIViewController,UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var btnSelectItem: UIButton!
    @IBOutlet weak var btn_menu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*
        if self.revealViewController() != nil {
            btn_menu.target = self.revealViewController()
            btn_menu.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
 */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSelectItem(_ sender: Any) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "PopOverSuggestionCategory") as! SuggestionCategoryTableViewController
        VC.preferredContentSize = CGSize(width: UIScreen.main.bounds.width, height: 150)
        let navcontroller = UINavigationController(rootViewController: VC)
        navcontroller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popOver = navcontroller.popoverPresentationController
        popOver?.delegate = self
        let viewForSource = sender as! UIView
        popOver?.sourceView = viewForSource
        self.present(navcontroller,animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func selectSuggestionCategory(segue:UIStoryboardSegue){
        if let popOver = segue.source as? SuggestionCategoryTableViewController,
            let select_item:String = popOver.selectedItem{
            self.btnSelectItem.setTitle(select_item, for: UIControlState.normal)
         
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
