//
//  ProfileSignUpViewController.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 18/11/16.
//  Copyright © 2016 formando. All rights reserved.
//


import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AVFoundation

class ProfileSignUpViewController: UIViewController, FBSDKLoginButtonDelegate {
     let loginButton: FBSDKLoginButton = FBSDKLoginButton()
    
    @IBOutlet weak var profilePicture: FBSDKProfilePictureView!
    
    @IBOutlet weak var loginButtonContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //profilePicture.layer.cornerRadius = profilePicture.frame.width/2
        profilePicture.clipsToBounds = true
        loginButton.delegate = self
        loginButtonContainer.addSubview(loginButton)
        
        //loginButton.frame=CGRectMake(70, 500, 275, 50)
        loginButton.readPermissions = ["public_profile","user_friends", "email","user_about_me","user_birthday","user_location","user_friends"]
        
        // Do any additional setup after loading the view.
        
    }
    
    func updateLoginFacebook()
    {
        if FBSDKAccessToken.current() != nil
        {
            /*
            Utility.showLoadingScreen(self.view)
            //parameters
            //["fields":"id,email,name,picture.width(480).height(480)"]
            FBSDKGraphRequest(graphPath: "me", parameters: nil).startWithCompletionHandler { (graphConnectionRequest, result, error) -> Void in
                
                Utility.userFacebookID = (result.valueForKey("id")! as! String)
                Utility.userFacebookName = (result.valueForKey("name")! as! String)
                
                self.navigationTitle.title = "Energy Consumption Awareness"
                self.toogleGuestAndRegister(TypeUser.Oculer)
                Utility.hideLoadingScreen()
                print(Utility.userFacebookID!)
                print(Utility.userFacebookName!)
                Utility.myfacebookPicture = Utility.getProfilePicture(Utility.userFacebookID!)
                //self.testRef.image = self.getProfilePicture(FB_ID)
                
            }
            */
        }
        else
        {
            //self.navigationTitle.title = "You are Logged Out"
           // self.navigationTitle.title = "Guess"
            
        }
        
    }

    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        
        print("User Logged In")
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
            }
        }
        
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        
        graphRequest.start { (graphConnectionRequest, result, error) -> Void in
            
        }
        
        updateLoginFacebook()
        
    }
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
