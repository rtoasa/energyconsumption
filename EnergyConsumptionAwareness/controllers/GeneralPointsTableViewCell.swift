//
//  GeneralPointsTableViewCell.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 28/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit
import KFSwiftImageLoader
class GeneralPointsTableViewCell: UITableViewCell {

    @IBOutlet weak var image_people: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var points: UILabel!
    
    var generalPoints:GeneralPoints? {
        didSet {
            self.name.text = generalPoints?.name
            self.points.text = (generalPoints?.points as? String)
            if let image = generalPoints?.image {
                self.image_people.loadImage(urlString: "\(Server.server_image_person + image)")
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
