//
//  QuestionDetailTableViewCell.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit

class QuestionDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var option: UILabel!
    
    var questionOptio:QuestionOptions? {
        didSet {
            self.option.text = questionOptio?.option
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
