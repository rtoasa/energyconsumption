//
//  QuestionDetailViewController.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit
import CoreData

class QuestionDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var tableView: UITableView!

    
    var idCell : Int  = 0
    var items : [QuestionOptions] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self 
         // Do any additional setup after loading the view.
        
 
        //items = ArchivingRepository.repository.questio[idQuestion].questionOptions
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! QuestionDetailTableViewCell
        cell.questionOptio = items[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let id:Int32  = Int32(items[indexPath.row].idQuestion_option)
        self.storeValue(id: id,value_required: "value required")
        return indexPath
    }
    
    
    
    
    func storeValue(id: Int32, value_required: String) {
       let answerClassName:String = String(describing: Answer.self)
        let answer:Answer = NSEntityDescription.insertNewObject(forEntityName: answerClassName, into: DataBaseController.getContext()) as! Answer
        answer.id_question = id
        answer.value_required  = value_required
        DataBaseController.saveContext()
    }
    
    func updateQuestion(){
        //dice1 = arc4random_uniform(6) + 1;
        /*
        for questionArray in ArchivingRepository.repository.questio {
            if questionArray.idQuestion == self.idQuestion {
                items = questionArray.questionOptions
                return
            }
        }*/
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
