//
//  QuestionViewController.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class QuestionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //temp vars
    var cat_name:String = ""
    var cat_image:String = ""
    /*
    var idCategory:Int = 0 {
        didSet {
            for categoryArray in ArchivingRepository.repository.categ {
                if categoryArray.id == self.idCategory {
                    items = categoryArray.questions
                    return
                }
            }
        }
    }*/
    var idCell:Int = 0
    var items : [Questions] =  []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title_category: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.isEditing = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title_category.text = self.cat_name
        self.image.loadImage(urlString: self.cat_image)
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! QuestionTableViewCell
        cell.questio = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        self.idCell = indexPath.row
        return indexPath
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="segueQuestionsDetail"){
            let detailVC=segue.destination as! QuestionDetailViewController;
            detailVC.items = items[idCell].questionOptions
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
