//
//  SettingUpdateViewController.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit

class SettingUpdateViewController: UIViewController {

   
    @IBAction func SyncInformation(_ sender: Any) {
        Background.updateAnswer()
    }
   
    @IBAction func updateDataBase(_ sender: Any) {
        EnergyConsumptionClien.fetchLanguage()
        EnergyConsumptionClien.fetchCategory()
        EnergyConsumptionClien.fetchGeneralPoints()
        EnergyConsumptionClien.fetchAdvices()
        
        
        //let alert = UIAlertController(title: AlertString.title, message: AlertString.message, preferredStyle: .actionSheet)
        /*alert.addAction(UIAlertAction(title: "Fair Skin", style: .default, handler: { (action) in
            //execute some code when this option is selected
        }))
        alert.addAction(UIAlertAction(title: "Dark Skin", style: .default, handler: { (action) in
            //execute some code when this option is selected
        }))*/
        //self.present(alert, animated: true, completion: nil)
    }
    private struct AlertString {
        static let title = NSLocalizedString("Title", comment: "Information")
        static let message = NSLocalizedString("Message", comment: "Data Base updated succesfull")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
