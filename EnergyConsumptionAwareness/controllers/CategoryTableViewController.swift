//
//  categoriesTableViewController.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 18/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class CategoryTableViewController: UITableViewController  {
    var idCategory : Int = 0
    var idCell : Int = 0
    @IBOutlet weak var btn_menu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //SWRevealViewController
        /*if self.revealViewController() != nil {
            btn_menu.target = self.revealViewController()
            btn_menu.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }*/

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArchivingRepository.repository.categ.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryTableViewCell
        cell.categ = ArchivingRepository.repository.categ[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        self.idCategory = ArchivingRepository.repository.categ[indexPath.row].id as! Int
        self.idCell = indexPath.row
        return indexPath
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segueQuestions"){
            let detailVC = segue.destination as! QuestionViewController;
            detailVC.items = ArchivingRepository.repository.categ[idCell].questions
            detailVC.cat_name = ArchivingRepository.repository.categ[idCell].category
            /*if let image = ArchivingRepository.repository.categ[idCell].cat_image {
                detailVC.cat_image = "\(Server.server_image_category + image)"
            }*/
            
            
        }
    }
    
    
}
