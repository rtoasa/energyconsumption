//
//  Questionnarie+JSON.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 26/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

extension Questions {
    static func parse(json:[String:Any]) -> Questions? {
        
        guard let idQuestion = json["id_question"] as? Int else {
            return nil
        }
        guard let question = json["question"] as? String else {
            return nil
        }
        guard let idVersion = json["id_version"] as? Int else {
            return nil
        }
        guard let idQuestionType = json["id_question_type"] as? Int else {
            return nil
        }
        guard let cantOptionRequired = json["cant_option_required"] as? Int else {
            return nil
        }
                guard let idLanguage = json["id_language"] as? Int else {
            return nil
        }
        guard let active = json["active"] as? Int else {
            return nil
        }
        guard let points = json["points"] as? Int else {
            return nil
        }
        
        var questionOptio = [QuestionOptions]()
        if let jsonArray = json["question_options"] as? [[String:Any]] {
            for jsonQuestion in jsonArray {
                let question = QuestionOptions.parse(json: jsonQuestion)
                let newQuestion = question
                questionOptio.append(newQuestion!)
                
            }
        }
        
        guard let questionOptions = questionOptio as? [QuestionOptions] else {
            return nil
        }
        /*if (questionOptio == nil) {
            let questionOptions = [QuestionOptions]()
        }*/
        let quesions = Questions(idQuestion:idQuestion, question: question , idVersion: idVersion, idQuestionType: idQuestionType,questionOptions: questionOptions, cantOptionRequired: cantOptionRequired, idLanguage: idLanguage, active:active, points: points
        )
        return quesions
    }
}
