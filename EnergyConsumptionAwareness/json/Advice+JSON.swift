//
//  Advice.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 26/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

extension Advice {
    static func parse(json:[String:Any]) -> Advice? {
        guard let idAdvice = json["id_advice"] as? Int else {
            return nil
        }
        guard let advice = json["advice"] as? String else {
            return nil
        }
        
        guard let dateTime = json["registration_date"] as? String else {
            return nil
        }
       
        let advices = Advice(idAdvice: idAdvice, advice: advice, dateTime: dateTime)
        return advices
    }
}
