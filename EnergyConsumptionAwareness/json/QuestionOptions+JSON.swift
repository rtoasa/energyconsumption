//
//  QuestionOptions+JSON.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 26/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
extension QuestionOptions {
    static func parse(json:[String:Any]) -> QuestionOptions? {
        guard let idQuestion_option = json["id_question_option"] as? Int else {
            return nil
        }
        guard let option = json["option"] as? String else {
            return nil
        }
        guard let idType = json["id_type"] as? Int else {
            return nil
        }
        let questionOptions = QuestionOptions(idQuestion_option:idQuestion_option, option:option, idType:idType)
        return questionOptions
    }
}
