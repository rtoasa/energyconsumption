//
//  GeneralPoints+JSON.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 28/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
extension GeneralPoints {
    static func parse(json:[String:Any]) -> GeneralPoints? {
        guard let id = json["id_person"] as? Int else {
            return nil
        }
        guard let name = json["name"] as? String else {
            return nil
        }
        guard let points = json["cant_points"] as? Int else {
            return nil
        }
        guard let image = json["image"] as? String else {
            return nil
        }
        
        let people = GeneralPoints(id: id, name: name,points: points, image: image)
        return people
    }
}
