//
//  Profile+CoreDataProperties.swift
//  
//
//  Created by formando on 14/12/16.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile");
    }

    @NSManaged public var idDevice: String?
    @NSManaged public var id_person: Int32
    @NSManaged public var idFacebook: String?
    @NSManaged public var token: String?

}
