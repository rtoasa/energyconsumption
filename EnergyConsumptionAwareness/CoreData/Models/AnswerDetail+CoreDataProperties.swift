//
//  AnswerDetail+CoreDataProperties.swift
//  
//
//  Created by formando on 14/12/16.
//
//

import Foundation
import CoreData


extension AnswerDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AnswerDetail> {
        return NSFetchRequest<AnswerDetail>(entityName: "Answer_detail");
    }

    @NSManaged public var id_question_option: Int32
    @NSManaged public var id_answer: Int32

}
