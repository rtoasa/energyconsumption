//
//  Setting+CoreDataProperties.swift
//  
//
//  Created by formando on 14/12/16.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Setting {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Setting> {
        return NSFetchRequest<Setting>(entityName: "Setting");
    }

    @NSManaged public var id_language: Int32
    @NSManaged public var id_version: Int32
    @NSManaged public var notification: String?
    @NSManaged public var sync_Datos: Int32
    @NSManaged public var presentation: Int32

}
