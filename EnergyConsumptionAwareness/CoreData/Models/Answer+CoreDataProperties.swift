//
//  Answer+CoreDataProperties.swift
//  
//
//  Created by formando on 14/12/16.
//
//

import Foundation
import CoreData


extension Answer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Answer> {
        return NSFetchRequest<Answer>(entityName: "Answer");
    }

    @NSManaged public var id_question: Int32
    @NSManaged public var value_required: String?
    @NSManaged public var id_answer: Int32

}
