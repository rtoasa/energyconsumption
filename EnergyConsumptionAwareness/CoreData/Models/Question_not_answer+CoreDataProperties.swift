//
//  Question_not_answer+CoreDataProperties.swift
//  
//
//  Created by formando on 14/12/16.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Question_not_answer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Question_not_answer> {
        return NSFetchRequest<Question_not_answer>(entityName: "Question_not_answer");
    }

    @NSManaged public var id_question: Int32

}
