//
//  EnergyConsumptionClient.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 24/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
class EnergyConsumptionClien {
    // static func fetchCategory (completion: @escaping (Category?) -> Void) {
    static func fetchCategory () {
        if let url = URL(string: "\(Server.dir)category.php") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    //completion(nil)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let jsonArray = jsonDic["items"] as? [[String:Any]] {
                        for jsonCategory in jsonArray {
                            let category = Category.parse(json: jsonCategory)
                            let newCategory = category
                            ArchivingRepository.repository.categ.append(newCategory!)
                            
                           //completion(category)
                        }
                        ArchivingRepository.repository.saveCategory()
                    }
                    
                }
                //completion(nil)
            })
            dataTask.resume()
        }
        else {
            //completion(nil)
        }
    }
    static func fetchLanguage() {
        if let url = URL(string:"\(Server.dir)language.php") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    //completion(nil)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let jsonArray = jsonDic["items"] as? [[String:Any]] {
                        for jsonLanguage in jsonArray {
                            
                            let language = Language.parse(json: jsonLanguage)
                            let newLanguage = language
                            
                            ArchivingRepository.repository.language.append(newLanguage!)
                            
                        }
                        ArchivingRepository.repository.saveLanguage()
                    }
                }
                //completion(nil)
            })
            dataTask.resume()
        }
        else {
            //completion(nil)
        }
    }
    static func fetchAdvices() {
        if let url = URL(string:"\(Server.dir)advice.php") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    //completion(nil)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let jsonArray = jsonDic["items"] as? [[String:Any]] {
                        for jsonAdvice in jsonArray {
                             
                            let advice = Advice.parse(json: jsonAdvice)
                            let newAdvice = advice
                           
                            ArchivingRepository.repository.advic.append(newAdvice!)
                        
                        }
                        ArchivingRepository.repository.saveAdvices()
                    }
                }
                //completion(nil)
            })
            dataTask.resume()
        }
        else {
           //completion(nil)
        }
    }
    static func fetchGeneralPoints() {
        if let url = URL(string:"\(Server.dir)general_points.php") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    //completion(nil)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let jsonArray = jsonDic["items"] as? [[String:Any]] {
                        for jsonPeople in jsonArray {
                            let people = GeneralPoints.parse(json: jsonPeople)
                            let newPeople = people
                            ArchivingRepository.repository.generalPoint.append(newPeople!)
                            
                        }
                        ArchivingRepository.repository.saveGeneralPoints()
                    }
                }
                //completion(nil)
            })
            dataTask.resume()
        }
        else {
            //completion(nil)
        }
    }
    static func sendAnswer(valor: [String: Any] ){
        if let url = URL(string:"\(Server.dir)answer.php") {
            let request = NSMutableURLRequest(url:url);
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: valor, options: JSONSerialization.WritingOptions.prettyPrinted)
            }catch let error as NSError{
                print("error convirtiendo diccionario a json: \(error)")
            }
            
            request.httpMethod = "POST";
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            //print(request)
            let dataTask = URLSession.shared.dataTask(with: request  as URLRequest, completionHandler: { (data, response, error) in
                if data == nil {
                    //completion(nil)
                    print("Error to send data in background")
                    return
                }
                //print(data! as! String)
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let jsonArray = jsonDic["items"] as? [[String:Any]] {
                       print(jsonArray     )
                    }
                }
                //completion(nil)
            })
            dataTask.resume()
        }
        else {
            //completion(nil)
        }
    }
}
