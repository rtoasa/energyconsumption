//
//  BackgroundFetch.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 3/12/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
import CoreData

class Background  {
    static func updateAnswer () {
        let fetchRequest:NSFetchRequest<Answer> = Answer.fetchRequest()
        
        var jsonArray: [Answer] = []
        do{
            let searchResults = try DataBaseController.getContext().fetch(fetchRequest)
            print("number of results: \(searchResults.count)")
            for result in searchResults as [Answer] {
                jsonArray.append(result)
            }
        }
        catch{
            print("Error: \(error)")
        }
        
        var answersArray = [Any]()
        for result in jsonArray as [Answer] {
            let jsonAnswer: [String: Any] = [
                "id_answer" : result.id_question,
                "value_required": result.value_required
            ]
            answersArray.append(jsonAnswer)
        }
        let answersDic = ["answers" : answersArray]
        EnergyConsumptionClien.sendAnswer(valor: answersDic)
        
    }
}
