//
//  ProfileTableViewCell.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 4/12/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var cant: UILabel!
    
    var pointDetail:ProfilePointsDetail? {
        didSet {
            self.title.text = pointDetail?.title
            //self.cant.text = pointDetail?.cant as! String
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
