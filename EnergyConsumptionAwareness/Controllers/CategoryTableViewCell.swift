//
//  categoriesTableViewCellController.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 24/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import UIKit
import Foundation
import KFSwiftImageLoader

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descrip: UILabel!
    @IBOutlet weak var cat_img: UIImageView!
    
    var categ:Category? {
        didSet {
            self.name.text = categ?.category
            self.descrip.text = categ?.cat_description
            if let image = categ?.cat_image {
                self.cat_img.loadImage(urlString: "\(Server.server_image_category + image)")
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
