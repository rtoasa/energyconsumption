//
//  Category.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 19/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

class Category:NSObject, NSCoding {
    var id:Int
    var category:String
    var cat_description:String
    var cat_image:String
    var questions:[Questions]
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(category, forKey: "category")
        aCoder.encode(cat_description, forKey: "description")
        aCoder.encode(cat_image, forKey: "image")
        aCoder.encode(questions, forKey: "questions")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeInteger(forKey: "id")
        category = aDecoder.decodeObject(forKey: "description") as! String
        cat_description = aDecoder.decodeObject(forKey: "description") as! String
        cat_image = aDecoder.decodeObject(forKey: "image") as! String
        questions = aDecoder.decodeObject(forKey: "questions") as! [Questions]
    }
    
    init(id:Int, category:String, cat_description:String, cat_image:String, questions:[Questions]) {
        self.id = id
        self.category = category
        self.cat_description = cat_description
        self.cat_image = cat_image
        self.questions = questions
    }
    
}

/*
struct Category {
    let name: String
    let description: String
}*/
