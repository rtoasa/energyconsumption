//
//  Questionnaire.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 24/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

class Questions: NSObject, NSCoding {
    
    var idQuestion:Int
    var question:String
    var idVersion:Int
    var idQuestionType:Int
    var questionOptions:[QuestionOptions]
    var cantOptionRequired:Int
    var idLanguage:Int
    var active:Int
    var points:Int
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(idQuestion, forKey: "id_question")
        aCoder.encode(question, forKey: "question")
        aCoder.encode(idVersion, forKey: "id_version")
        aCoder.encode(idQuestionType, forKey: "id_question_type")
        aCoder.encode(questionOptions, forKey: "question_options")
        aCoder.encode(cantOptionRequired, forKey: "cant_option_required")
        aCoder.encode(idLanguage, forKey: "id_language")
        aCoder.encode(active, forKey: "active")
        aCoder.encode(points, forKey: "points")
    }
    
    required init?(coder aDecoder: NSCoder) {
        idQuestion = aDecoder.decodeInteger(forKey: "id_question")
        question = aDecoder.decodeObject(forKey: "question") as! String
        idVersion = aDecoder.decodeInteger(forKey: "id_version")
        idQuestionType = aDecoder.decodeInteger(forKey: "id_question_type")
        questionOptions = aDecoder.decodeObject(forKey: "question_options") as! [QuestionOptions]
        cantOptionRequired = aDecoder.decodeInteger(forKey: "cant_option_required")
        idLanguage = aDecoder.decodeInteger(forKey: "id_language")
        active = aDecoder.decodeInteger(forKey: "active")
        points = aDecoder.decodeInteger(forKey: "points")
    }
    
    init(idQuestion:Int, question:String ,idVersion:Int, idQuestionType:Int, questionOptions:[QuestionOptions],cantOptionRequired:Int, idLanguage:Int, active:Int, points:Int) {
        self.idQuestion = idQuestion
        self.question = question
        self.idVersion = idVersion
        self.idQuestionType = idQuestionType
        self.questionOptions = questionOptions
        self.cantOptionRequired = cantOptionRequired
        self.idLanguage = idLanguage
        self.active = active
        self.points = points
    }
    
}
