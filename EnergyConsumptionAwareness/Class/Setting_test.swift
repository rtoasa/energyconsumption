//
//  Setting.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

class Setting_test:NSObject, NSCoding {
    
    var inicio:String
   
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(inicio, forKey: "inicio")
    }
    
    required init?(coder aDecoder: NSCoder) {
        inicio = aDecoder.decodeObject(forKey: "inicio") as! String
    }
    
    init(inicio:String) {
        self.inicio = inicio
    }
    
}
