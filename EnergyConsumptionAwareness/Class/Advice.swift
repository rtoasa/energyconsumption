//
//  Advice.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 26/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
class Advice:NSObject, NSCoding {
    
    var idAdvice:Int
    var advice:String
    var dateTime:String
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(idAdvice, forKey: "id_advice")
        aCoder.encode(advice, forKey: "advice")
        aCoder.encode(dateTime, forKey: "registratio_date")
    }
    
    required init?(coder aDecoder: NSCoder) {
      
        idAdvice = aDecoder.decodeInteger(forKey: "id_advice")
        advice = aDecoder.decodeObject(forKey: "advice") as! String
        dateTime = aDecoder.decodeObject(forKey: "registratio_date") as! String
    }
    
    init(idAdvice:Int, advice:String, dateTime:String) {
        self.idAdvice = idAdvice
        self.advice = advice
        self.dateTime = dateTime
    }
    
}
