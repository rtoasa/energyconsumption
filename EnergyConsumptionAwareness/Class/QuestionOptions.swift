//
//  Questions.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 26/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
class QuestionOptions: NSObject, NSCoding {
    
    var idQuestion_option:Int
    var option : String
    var idType:Int
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(idQuestion_option, forKey: "id_question_option")
        aCoder.encode(option, forKey: "option")
        aCoder.encode(idType, forKey: "id_type")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        idQuestion_option = aDecoder.decodeInteger(forKey: "id_question_option")
        option = aDecoder.decodeObject(forKey: "option") as! String
        idType = aDecoder.decodeInteger(forKey: "id_type")
    }
    
    init(idQuestion_option:Int, option:String, idType:Int) {
        self.idQuestion_option = idQuestion_option
        self.option = option
        self.idType = idType
    }
    
}
