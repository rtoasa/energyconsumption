//
//  Language.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 16/12/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
class Language:NSObject, NSCoding {
    var id:Int
    var language:String
    var languageCode:String

    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(languageCode, forKey: "language_code")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeInteger(forKey: "id")
        language = aDecoder.decodeObject(forKey: "language") as! String
        languageCode = aDecoder.decodeObject(forKey: "language_code") as! String
    }
    
    init(id:Int, language:String, languageCode:String) {
        self.id = id
        self.language = language
        self.languageCode = languageCode
    }
    
}
