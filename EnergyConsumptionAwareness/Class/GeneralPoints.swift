//
//  GeneralPoints.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 27/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
class GeneralPoints:NSObject, NSCoding {
    
    var id:Int
    var name:String
    var points:Int
    var image:String
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
         aCoder.encode(points, forKey: "points")
        aCoder.encode(image, forKey: "image")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeInteger(forKey: "id")
        name = aDecoder.decodeObject(forKey: "name") as! String
        points = aDecoder.decodeInteger(forKey: "points")
        image = aDecoder.decodeObject(forKey: "image") as! String
    }
    
    init(id:Int, name:String, points:Int, image:String) {
        self.id = id
        self.name = name
        self.points = points
        self.image = image
    }
 
}
