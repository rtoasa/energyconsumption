//
//  ProfilePointsDetail.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 4/12/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation

class ProfilePointsDetail:NSObject, NSCoding {
    
    var title:String
    var cant:Int
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(cant, forKey: "cant")
    }
    
    required init?(coder aDecoder: NSCoder) {
        title = aDecoder.decodeObject(forKey: "title") as! String
        cant = aDecoder.decodeInteger(forKey: "cant")
    }
    
    init(title: String, cant: Int) {
        self.title = title
        self.cant = cant
    }
    
}
