//
//  Language+JSON.swift
//  EnergyConsumptionAwareness
//
//  Created by Carlos Alfredo Silva Villafuerte on 16/12/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
extension Language {
    static func parse(json:[String:Any]) -> Language? {
        guard let id = json["id_language"] as? Int else {
            return nil
        }
        guard let languag = json["language"] as? String else {
            return nil
        }
        guard let languagecod = json["language_code"] as? String else {
            return nil
        }
        let language = Language(id: id, language: languag, languageCode: languagecod)
        return language
    }
}
