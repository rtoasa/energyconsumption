//
//  Category+JSON.swift
//  EnergyConsumptionAwareness
//
//  Created by formando on 24/11/16.
//  Copyright © 2016 formando. All rights reserved.
//

import Foundation
extension Category {
    static func parse(json:[String:Any]) -> Category? {
        guard let id = json["id_category"] as? Int else {
            return nil
        }
        guard let name = json["category"] as? String else {
            return nil
        }
        guard let description = json["description"] as? String else {
            return nil
        }
        guard let image = json["image"] as? String else {
            return nil
        }
        var questions = [Questions]()
        if let jsonArray = json["questions"] as? [[String:Any]] {
            for jsonQuestion in jsonArray {
                let question = Questions.parse(json: jsonQuestion)
                let newQuestion = question
                questions.append(newQuestion!)
                
            }
        }

        let category = Category(id: id, category: name, cat_description: description, cat_image: image,questions: questions)
        return category
    }
}
